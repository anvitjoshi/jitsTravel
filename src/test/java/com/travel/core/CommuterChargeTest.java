package com.travel.core;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class CommuterChargeTest {
	private CommuterCharges cc;
	
	@Before
	public void setUp() {
		cc = new CommuterCharges();
	}
	
	@Test
	public void commuterChargeRiderCardTest() {
		double actual = cc.calTravelCost(3, true);
		double expected = 1.35;
		assertEquals(expected, actual, 0.01);
	}
	
	@Test
	public void commuterChargeNoRiderCardTest() {
		double actual = cc.calTravelCost(4, false);
		double expected = 2.0;
		assertEquals(expected, actual, 0.01);
	}
	
	@Test
	public void commuterChargeFalseCaseTest() {
		double actual = cc.calTravelCost(3, true);
		double unexpected = 1.30;
		assertNotEquals(unexpected, actual, 0.01);
	}

}
