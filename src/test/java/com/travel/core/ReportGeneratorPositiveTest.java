package com.travel.core;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class ReportGeneratorPositiveTest {
	
	private List<Passenger> passengers;
	private ReportGenerator rg;
	
	@Before
	public void setUp() {
		passengers = new ArrayList<>();
		rg = new ReportGenerator();
		Passenger c1 = new Passenger("commuter", 0, 3, true, false);
		Passenger c2 = new Passenger("commuter", 0, 5, true, false);
		Passenger c3 = new Passenger("commuter", 0, 4, false, true);
		Passenger v1 = new Passenger("vacationer", 90, 0, false, false);
		Passenger v2 = new Passenger("vacationer", 199, 0, false, true);
		
		passengers.add(c1);
		passengers.add(c2);
		passengers.add(c3);
		passengers.add(v1);
		passengers.add(v2);
	}
	
	@Test
	public void numNewsPapersTest() {
		int expected = 2;
		int actual = rg.numNewsPapers(passengers);
		assertEquals(expected, actual);
	}
	
	@Test
	public void numMealsTest() {
		int expected = 3;
		int actual = rg.numMeals(passengers);
		assertEquals(expected, actual);
	}
	
	@Test
	public void totalCostOfTripTest() {
		double expected = 150.1;
		double actual = rg.totalCostOfTrip(passengers);
		assertEquals(expected, actual, 0.01);
	}

}
