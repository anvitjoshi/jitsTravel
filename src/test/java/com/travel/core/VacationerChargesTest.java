package com.travel.core;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class VacationerChargesTest {
	
	private VacationerCharges vc;
	
	@Before
	public void setUp() {
		vc = new VacationerCharges();
	}
	
	@Test
	public void calTravelCostPositiveTest() {
		double actual = vc.calTravelCost(90, true);
		double expected = 45.0;
		assertEquals(expected, actual, 0.01);
	}
	
	@Test
	public void calTravelCostNegativeTest() {
		double actual = vc.calTravelCost(199, true);
		double unexpected = 45.0;
		assertNotEquals(unexpected, actual, 0.01);
	}
	
	@Test
	public void mealCountPositiveTest() {
		int actual = vc.mealCount(199);
		int expected = 2;
		assertEquals(expected, actual);
	}
	
	@Test
	public void mealCountCostNegativeTest() {
		double actual = vc.mealCount(90);
		double unexpected = 45;
		assertNotEquals(unexpected, actual);
	}

}
