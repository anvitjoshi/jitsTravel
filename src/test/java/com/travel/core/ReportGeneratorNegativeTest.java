package com.travel.core;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class ReportGeneratorNegativeTest {
	
	private List<Passenger> passengers;
	private ReportGenerator rg;
	
	@Rule public ExpectedException thrown = ExpectedException.none();
	
	@Before
	public void setUp() {
		passengers = new ArrayList<>();
		rg = new ReportGenerator();
//		Passenger c1 = new Passenger("commuter", 0, 3, true, false);
//		Passenger c2 = new Passenger("commuter", 0, 5, true, false);
//		Passenger c3 = new Passenger("commuter", 0, 4, false, true);
//		Passenger v1 = new Passenger("vacationer", 90, 0, false, false);
//		Passenger v2 = new Passenger("vacationer", 199, 0, false, true);
		
	}
	
	@Test
	public void lowMilesTest() {
		thrown.expect(IllegalArgumentException.class);
		thrown.expectMessage("Number of miles must be between 5 and 4000");
		new Passenger("vacationer", 4, 0, false, false);
	}
	
	@Test
	public void highMilesTest() {
		thrown.expect(IllegalArgumentException.class);
		thrown.expectMessage("Number of miles must be between 5 and 4000");
		new Passenger("vacationer", 4001, 0, false, false);
	}
	
	@Test
	public void numberOfStopsTest() {
		thrown.expect(IllegalArgumentException.class);
		thrown.expectMessage("Number of stops must be greater than Zero");
		new Passenger("commuter", 40, 0, false, false);
	}
	
	@Test
	public void passengerTypeTest() {
		thrown.expect(IllegalArgumentException.class);
		thrown.expectMessage("Passenger type must be either Commuter or Vacationer");
		new Passenger("something", 400, 0, false, false);
	}
	
	

}
