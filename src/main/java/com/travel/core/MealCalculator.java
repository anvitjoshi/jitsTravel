package com.travel.core;

public interface MealCalculator {
	int mealCount(int miles);
}
