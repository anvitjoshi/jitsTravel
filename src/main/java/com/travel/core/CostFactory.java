package com.travel.core;

public abstract class CostFactory {
	
	public static TravelCharges getObject(String passengerType) {
		switch(passengerType) {
			case "commuter":
				return new CommuterCharges();
			case "vacationer":	
				return new VacationerCharges();
			default:
				return null;
		}
	}
}
