package com.travel.core;

import java.util.List;

public class ReportGenerator {
	
	int numNewsPapers(List<Passenger> passengers) {
		int totalNewsPapers = 0;
		for (Passenger p: passengers) {
			if(p.isNewspaper()) {
				totalNewsPapers++;
			}
		}
		return totalNewsPapers;
	}
	
	int numMeals(List<Passenger> passengers) {
		int totalMeals = 0;
		for (Passenger p: passengers) {
			String type = p.getPassengerType();
			if(!type.equalsIgnoreCase("commuter")) {
				MealCalculator mc = MealFactory.getObject(type);
				int curMeal = mc.mealCount(p.getMiles());
				p.setMeals(curMeal);
				totalMeals += curMeal;
			}
		}
		return totalMeals;
	}
	
	double totalCostOfTrip(List<Passenger> passengers) {
		double totalCost = 0;
		for (Passenger p: passengers) {
			String type = p.getPassengerType();
			
			int milesOrStops = 0;
			if (type.equalsIgnoreCase("vacationer")) {
				milesOrStops = p.getMiles();
			}
			else if(type.equalsIgnoreCase("commuter")) {
				milesOrStops = p.getStops();
			}
			TravelCharges tc = CostFactory.getObject(type);
			double curCost = tc.calTravelCost(milesOrStops, p.isRiderCard());
			p.setTotalCost(curCost);
			totalCost += curCost;
		}
		return totalCost;
	}
}
