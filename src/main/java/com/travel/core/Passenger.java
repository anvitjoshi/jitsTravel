package com.travel.core;

public class Passenger {

	private String passengerType;
	private int miles = 0;
	private int stops = 0;
	private boolean riderCard = false;
	private boolean newspaper = true;
	private int meals = 0;
	double totalCost;
	
	public Passenger(String passengerType, int miles, int stops, boolean riderCard, boolean newspaper) {
		super();
		
		this.passengerType = passengerType.toLowerCase();
		this.setNewspaper(newspaper);
		
		if(!this.passengerType.equalsIgnoreCase("vacationer") 
				&& !this.passengerType.equalsIgnoreCase("commuter") ) {
			throw new IllegalArgumentException("Passenger type must be either Commuter or Vacationer");
		}
		
		if(this.passengerType.equalsIgnoreCase("vacationer")) {
			if(miles < 5 || miles > 4000) {
				throw new IllegalArgumentException("Number of miles must be between 5 and 4000");
			}
			this.miles = miles;
		}
		else if(this.passengerType.equalsIgnoreCase("commuter")){
			if(stops < 1) {
				throw new IllegalArgumentException("Number of stops must be greater than Zero");
			}
			this.stops = stops;
			this.setRiderCard(riderCard);			
		}
		
	}

	public String getPassengerType() {
		return passengerType;
	}
	
	public int getMiles() {
		return miles;
	}
	
	public int getStops() {
		return stops;
	}
	
	public boolean isRiderCard() {
		return riderCard;
	}
	
	public void setRiderCard(boolean riderCard) {
		this.riderCard = riderCard;
	}
	
	public boolean isNewspaper() {
		return newspaper;
	}
	public void setNewspaper(boolean newspaper) {
		this.newspaper = newspaper;
	}
	
	public int getMeals() {
		return meals;
	}
	
	public void setMeals(int meals) {
		this.meals = meals;
	}
	
	public double getTotalCost() {
		return totalCost;
	}

	public void setTotalCost(double totalCost) {
		this.totalCost = totalCost;
	}

}
