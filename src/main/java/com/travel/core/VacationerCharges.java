package com.travel.core;

public class VacationerCharges implements TravelCharges, MealCalculator {

	@Override
	public double calTravelCost(int miles, boolean riderCard) {
		double cost;
		cost = 0.5 * miles;
		
		return cost;
	}
	
	@Override
	public int mealCount(int miles) {
		return ((miles + 99) / 100 );
	}
}
