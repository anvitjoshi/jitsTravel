package com.travel.core;

public class CommuterCharges implements TravelCharges {

	@Override
	public double calTravelCost(int stops, boolean riderCard) {
		double cost;
		cost = 0.5 * stops;
		
		if(riderCard) {
			cost *= 0.9;
		}
		
		return cost;
	}

}
