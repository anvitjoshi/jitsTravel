package com.travel.core;

public abstract class MealFactory {
	
	public static MealCalculator getObject(String passengerType) {
		if(passengerType.equalsIgnoreCase("VACATIONER")) {
			return new VacationerCharges();
		}
		else {
			return null;
		}
	}
}
