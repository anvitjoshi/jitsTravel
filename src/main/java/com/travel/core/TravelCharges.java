package com.travel.core;

public interface TravelCharges {
	double calTravelCost(int milesOrStops, boolean riderCard);
}
